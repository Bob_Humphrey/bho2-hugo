var b = function( sketch ) {

  var img;
  var imgSource = '/computer-art/37.jpg';
  var container = 'p5-line-by-line';
  var originalImageHeight = 1;
  var originalImageWidth = 1;
  var lineSeparation = 100;
  var startingLineWidth = 15;
  var densityDirection = 'higher';
  var slowTransormSpeed = 1250;
  var fastTransformSpeed = 250;
  var fastTransformLineSeparation = 22;
  var completeImageDelay = 70;
  var resetCompletImageDelay = 70;
  var imageWidth;
  var imageHeight;
  var prevMills = 0;
  var diffMills = 0;



  sketch.preload = function() {
    img = sketch.loadImage(imgSource);
    sketch.pixelDensity(1);
  }

  sketch.setup = function() {
    imageWidth = document.getElementById('p5-line-by-line').offsetWidth;
    imageHeight = Math.floor( imageWidth * originalImageHeight / originalImageWidth);
    manipulateImage( lineSeparation, img, imageWidth, imageHeight, container );
  }

  sketch.draw = function() {
    // Redraw the image based on how many milliseconds have passed since the
    // script started running. There is a fast transform interval to cause
    // the image to be redrawn more often at lower resolutions, and a slow transform
    // interval to redraw the screen less often at higher resolutions.
    // The densityDirection is set to 'lower' when each redraw will be at a
    // lower resolution than the last image. When the resolution gets to the
    // maxiumum or minimum, the direction is reversed.
    let mills = sketch.millis();
    diffMills = mills - prevMills;
    if ((( lineSeparation >= fastTransformLineSeparation ) && ( diffMills > fastTransformSpeed )) ||
      (( lineSeparation < fastTransformLineSeparation ) && ( diffMills > slowTransormSpeed ))) {
      prevMills = mills;
      if (densityDirection === 'higher') {
        lineSeparation--;
        if (lineSeparation < startingLineWidth) {
          lineSeparation = startingLineWidth;
          if (completeImageDelay) {
            completeImageDelay--;
          }
          else {
            densityDirection = 'lower';
            completeImageDelay = resetCompletImageDelay;
          }
        }
        manipulateImage( lineSeparation, img, imageWidth, imageHeight, container );
      }
      else if (densityDirection === 'lower'){
        lineSeparation++;
        if (lineSeparation > 100 ) {
          lineSeparation = 100;
          densityDirection = 'higher';
        }
        manipulateImage( lineSeparation, img, imageWidth, imageHeight, container );
      }
    }
  }

  function manipulateImage( lineSeparation, img, imageWidth, imageHeight, container) {
    let manipulatedImage = sketch.createCanvas(imageWidth, imageHeight);
    manipulatedImage.parent(container);
    let lineSeparationImageLength = imageWidth * imageHeight * 4;
    let lineSeparationPixels = new Array(lineSeparationImageLength).fill(0);


    sketch.image(img, 0, 0, imageWidth, imageHeight);
    sketch.loadPixels();

    for (let y = 0; y < imageHeight; y++ ) {
      for (let x = 0; x < imageWidth; x++ ) {
        let index = ( x + y * imageWidth ) * 4;
        let r = sketch.pixels[index + 0];
        let b = sketch.pixels[index + 1];
        let g = sketch.pixels[index + 2];
        let brightness = (r + b + g) / 3;
        if ((x + y) % lineSeparation < startingLineWidth ) {
          lineSeparationPixels[index + 0] = r;
          lineSeparationPixels[index + 1] = b;
          lineSeparationPixels[index + 2] = g;
          lineSeparationPixels[index + 3] = 255;
        }
      }
    }
    for (let z = 0; z < lineSeparationPixels.length; z++ ) {
      sketch.pixels[z] = lineSeparationPixels[z];
    }
    sketch.updatePixels();
  }

};

var myp5 = new p5(b, 'p5-line-by-line');
