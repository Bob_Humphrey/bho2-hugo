const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const del = require('del');

// photography

const input1 = 'themes/bho2/static/img-resized/photography/*.*';
const output1 = 'themes/bho2/static/photography/';

del(['themes/bho2/static/photography/*']);

const optimiseJPEGImages1 = () =>
  imagemin([input1], output1, {
    plugins: [
      imageminMozjpeg({
        quality: 70,
        progressive: true
      }),
    ]
  });

optimiseJPEGImages1()
  .catch(error => console.log(error));

// randall library images

  const input2 = 'themes/bho2/static/img-resized/randall/**/*';
  const output2 = 'themes/bho2/static/img/randall/';

  del(['themes/bho2/static/img/randall/*']);

  const optimiseJPEGImages2 = () =>
    imagemin([input2], output2, {
      plugins: [
        imageminMozjpeg({
          quality: 70,
          progressive: true
        }),
      ]
    });

  optimiseJPEGImages2()
    .catch(error => console.log(error));

// roadway express images

  const input3 = 'themes/bho2/static/img-resized/rex/**/*';
  const output3 = 'themes/bho2/static/img/rex/';

  del(['themes/bho2/static/img/rex/*']);

  const optimiseJPEGImages3 = () =>
    imagemin([input3], output3, {
      plugins: [
        imageminMozjpeg({
          quality: 70,
          progressive: true
        }),
      ]
    });

  optimiseJPEGImages3()
    .catch(error => console.log(error));

// art images

  const input4 = 'themes/bho2/static/img-resized/art/**/*';
  const output4 = 'themes/bho2/static/art/';

  del(['themes/bho2/static/static/art/*']);

  const optimiseJPEGImages4 = () =>
    imagemin([input4], output4, {
      plugins: [
        imageminMozjpeg({
          quality: 70,
          progressive: true
        }),
      ]
    });

  optimiseJPEGImages4()
    .catch(error => console.log(error));

// computer art images

  const input5 = 'themes/bho2/static/img-resized/computer-art/**/*';
  const output5 = 'themes/bho2/static/computer-art/';

  del(['themes/bho2/static/computer-art/*']);

  const optimiseJPEGImages5 = () =>
    imagemin([input5], output5, {
      plugins: [
        imageminMozjpeg({
          quality: 70,
          progressive: true
        }),
      ]
    });

  optimiseJPEGImages5()
    .catch(error => console.log(error));
