var a = function( sketch ) {

  var img;
  var imgSource = '/computer-art/39.jpg';
  var container = 'p5-progressive-color';
  var originalImageHeight = 1;
  var originalImageWidth = 1;
  var threshold = 210;
  var densityDirection = 'higher';
  var slowTransormSpeed = 200;
  var fastTransformSpeed = 100;
  var fastTransformThreshold = 150;
  var completeImageDelay = 15;
  var resetCompletImageDelay = 15;
  var imageWidth;
  var imageHeight;
  var prevMills = 0;
  var diffMills = 0;



  sketch.preload = function() {
    img = sketch.loadImage(imgSource);
    sketch.pixelDensity(1);
  }

  sketch.setup = function() {
    imageWidth = document.getElementById('p5-progressive-color').offsetWidth;
    imageHeight = Math.floor( imageWidth * originalImageHeight / originalImageWidth);
    manipulateImage( threshold, img, imageWidth, imageHeight, container );
  }

  sketch.draw = function() {
    // Redraw the image based on how many milliseconds have passed since the
    // script started running. There is a fast transform interval to cause
    // the image to be redrawn more often at lower resolutions, and a slow transform
    // interval to redraw the screen less often at higher resolutions.
    // The densityDirection is set to 'lower' when each redraw will be at a
    // lower resolution than the last image. When the resolution gets to the
    // maxiumum or minimum, the direction is reversed.
    let mills = sketch.millis();
    diffMills = mills - prevMills;
    if ((( threshold >= fastTransformThreshold ) && ( diffMills > fastTransformSpeed )) ||
      (( threshold < fastTransformThreshold ) && ( diffMills > slowTransormSpeed ))) {
      prevMills = mills;
      if (densityDirection === 'higher') {
        threshold--;
        if (threshold < 1) {
          threshold = 1;
          if (completeImageDelay) {
            completeImageDelay--;
          }
          else {
            densityDirection = 'lower';
            completeImageDelay = resetCompletImageDelay;
          }
        }
        manipulateImage( threshold, img, imageWidth, imageHeight, container );
      }
      else if (densityDirection === 'lower'){
        threshold++;
        if (threshold > 255 ) {
          threshold = 255;
          densityDirection = 'higher';
        }
        manipulateImage( threshold, img, imageWidth, imageHeight, container );
      }
    }
  }

  function manipulateImage( threshold, img, imageWidth, imageHeight, container) {
    let manipulatedImage = sketch.createCanvas(imageWidth, imageHeight);
    manipulatedImage.parent(container);
    let thresholdImageLength = imageWidth * imageHeight * 4;
    let thresholdPixels = new Array(thresholdImageLength).fill(0);

    sketch.image(img, 0, 0, imageWidth, imageHeight);
    sketch.loadPixels();

    for (var y = 0; y < imageHeight; y++ ) {
      for (var x = 0; x < imageWidth; x++ ) {
        var index = ( x + y * imageWidth ) * 4;

        var r = sketch.pixels[index + 0];
        var b = sketch.pixels[index + 1];
        var g = sketch.pixels[index + 2];
        var brightness = (r + b + g) / 3;
        if (brightness >= threshold) {
          thresholdPixels[index + 0] = r;
          thresholdPixels[index + 1] = b;
          thresholdPixels[index + 2] = g;
          thresholdPixels[index + 3] = 255;
        }
        else {
          thresholdPixels[index + 3] = 0;
        }

      }
    }
    for (z = 0; z < thresholdPixels.length; z++ ) {
      sketch.pixels[z] = thresholdPixels[z];
    }
    sketch.updatePixels();
  }

};

var myp5 = new p5(a, 'p5-progressive-color');
