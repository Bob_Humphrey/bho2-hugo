var a = function( sketch ) {

  var img;
  var imgSource = '/computer-art/75.jpg';
  var container = 'p5-threshold';
  var originalImageHeight = 1;
  var originalImageWidth = 1;
  var threshold = 255.0;
  var step = 12.5;
  var posterizeStep = 50;
  var densityDirection = 'lower';
  var slowTransormSpeed = 1000;
  var fastTransformSpeed = 1000;
  var fastTransformThreshold = 150;
  var completeImageDelay = 0;
  var resetCompletImageDelay = 0;
  var imageWidth;
  var imageHeight;
  var prevMills = 0;
  var diffMills = 0;
  var stages = ["threshold", "posterize", "desaturated", "color", "pause"];
  var stage = 0;
  var currentStage = stages[stage];



  sketch.preload = function() {
    img = sketch.loadImage(imgSource);
    sketch.pixelDensity(1);
  }

  sketch.setup = function() {
    imageWidth = document.getElementById('p5-threshold').offsetWidth;
    imageHeight = Math.floor( imageWidth * originalImageHeight / originalImageWidth);
    manipulateImage( threshold, img, imageWidth, imageHeight, container );
  }

  sketch.draw = function() {
    // Cycle through different image effects contained in the table stages.
    // The effects are applied based on the image brightness of each pixel.

    let mills = sketch.millis();
    diffMills = mills - prevMills;
    if ((( threshold >= fastTransformThreshold ) && ( diffMills > fastTransformSpeed )) ||
      (( threshold < fastTransformThreshold ) && ( diffMills > slowTransormSpeed ))) {
      prevMills = mills;
      if (densityDirection === 'higher') {
        threshold -=step;
        if (threshold < 1 && currentStage === 'threshold') {
          threshold = 250;
          stage = 1;
          currentStage = stages[stage];
        }
        else if (threshold < 1 && currentStage === 'posterize') {
          threshold = 250;
          stage = 2;
          currentStage = stages[stage];
        }
        else if (threshold < 1 && currentStage === 'desaturated') {
          threshold = 250;
          stage = 3;
          currentStage = stages[stage];
        }
        else if (threshold < 1 && currentStage === 'color') {
          threshold = 250;
          stage = 4;
          currentStage = stages[stage];
        }
        else if (threshold < 1 && currentStage === 'pause') {
          threshold = 0;
          stage = 3;
          currentStage = stages[stage];
          densityDirection = 'lower';
        }
        manipulateImage( threshold, img, imageWidth, imageHeight, container );
      }
      else if (densityDirection === 'lower'){
        threshold +=step;
        if (threshold > 255 && currentStage === 'color') {
          threshold = 0;
          stage = 2;
          currentStage = stages[stage];
        }
        else if (threshold > 255 && currentStage === 'desaturated') {
          threshold = 0;
          stage = 1;
          currentStage = stages[stage];
        }
        else if  (threshold > 255 && currentStage === 'posterize') {
          threshold = 0;
          stage = 0;
          currentStage = stages[stage];
        }
        else if  (threshold > 255 && currentStage === 'threshold') {
          threshold = 250;
          stage = 0;
          currentStage = stages[stage];
          densityDirection = 'higher';
        }
        manipulateImage( threshold, img, imageWidth, imageHeight, container );
      }
    }
  }

  function manipulateImage( threshold, img, imageWidth, imageHeight, container) {
    let manipulatedImage = sketch.createCanvas(imageWidth, imageHeight);
    manipulatedImage.parent(container);
    let thresholdImageLength = imageWidth * imageHeight * 4;
    let thresholdPixels = new Array(thresholdImageLength).fill(0);

    sketch.image(img, 0, 0, imageWidth, imageHeight);
    sketch.loadPixels();

    for (var y = 0; y < imageHeight; y++ ) {
      for (var x = 0; x < imageWidth; x++ ) {
        var index = ( x + y * imageWidth ) * 4;

        var r = sketch.pixels[index + 0];
        var b = sketch.pixels[index + 1];
        var g = sketch.pixels[index + 2];
        var brightness = (r + b + g) / 3;

        if (currentStage === 'threshold') {
          if (brightness >= threshold) {
            thresholdPixels[index + 0] = 0;
            thresholdPixels[index + 1] = 0;
            thresholdPixels[index + 2] = 0;
            thresholdPixels[index + 3] = 255;
          }
          else {
            thresholdPixels[index + 3] = 0;
          }
        }

        if (currentStage === 'posterize') {
          if (brightness >= threshold) {
            thresholdPixels[index + 0] = Math.floor(brightness / posterizeStep) * posterizeStep;
            thresholdPixels[index + 1] = Math.floor(brightness / posterizeStep) * posterizeStep;
            thresholdPixels[index + 2] = Math.floor(brightness / posterizeStep) * posterizeStep;
            thresholdPixels[index + 3] = 255;
          }
          else {
            thresholdPixels[index + 0] = 0;
            thresholdPixels[index + 1] = 0;
            thresholdPixels[index + 2] = 0;
            thresholdPixels[index + 3] = 255;
          }
        }

        if (currentStage === 'desaturated') {
          if (brightness >= threshold) {
            thresholdPixels[index + 0] = brightness;
            thresholdPixels[index + 1] = brightness;
            thresholdPixels[index + 2] = brightness;
            thresholdPixels[index + 3] = 255;
          }
          else {
            thresholdPixels[index + 0] = Math.floor(brightness / posterizeStep) * posterizeStep;
            thresholdPixels[index + 1] = Math.floor(brightness / posterizeStep) * posterizeStep;
            thresholdPixels[index + 2] = Math.floor(brightness / posterizeStep) * posterizeStep;
            thresholdPixels[index + 3] = 255;
          }
        }

        if (currentStage === 'color') {
          if (brightness >= threshold) {
            thresholdPixels[index + 0] = r;
            thresholdPixels[index + 1] = b;
            thresholdPixels[index + 2] = g;
            thresholdPixels[index + 3] = 255;
          }
          else {
            thresholdPixels[index + 0] = brightness;
            thresholdPixels[index + 1] = brightness;
            thresholdPixels[index + 2] = brightness;
            thresholdPixels[index + 3] = 255;
          }
        }

        if (currentStage === 'pause') {
          thresholdPixels[index + 0] = r;
          thresholdPixels[index + 1] = b;
          thresholdPixels[index + 2] = g;
          thresholdPixels[index + 3] = 255;
        }
      }
    }
    for (z = 0; z < thresholdPixels.length; z++ ) {
      sketch.pixels[z] = thresholdPixels[z];
    }
    sketch.updatePixels();
  }

};

var myp5 = new p5(a, 'p5-threshold');
