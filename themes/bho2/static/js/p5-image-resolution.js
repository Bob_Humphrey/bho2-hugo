var t = function( sketch ) {

  var img;
  var imgSource = '/computer-art/82.jpg';
  var container = 'p5-image-resolution';
  var originalImageHeight = 1300;
  var originalImageWidth = 1300;
  var vScale = 80;
  var vScaleMax = 80;
  var densityDirection = 'higher';
  var slowTransormSpeed = 800;
  var fastTransformSpeed = 250;
  var fastTransformThreshold = 20;
  var completeImageDelay = 70;
  var resetCompletImageDelay = 70;

  var imageWidth;
  var imageHeight;
  var blocksPerRow;
  var blocksPerColumn;
  var adjustedXScale = 0.0;
  var adjustedYScale = 0.0;
  var prevMills = 0;
  var diffMills = 0;



  sketch.preload = function() {
    img = sketch.loadImage(imgSource);
    sketch.pixelDensity(1);
  }

  sketch.setup = function() {
    imageWidth = document.getElementById('p5-image-resolution').offsetWidth;
    imageHeight = Math.floor( imageWidth * originalImageHeight / originalImageWidth);
    manipulateImage( vScale, img, imageWidth, imageHeight, container );
  }

  sketch.draw = function() {
    // Redraw the image based on how many milliseconds have passed since the
    // script started running. There is a fast transform interval to cause
    // the image to be redrawn more often at lower resolutions, and a slow transform
    // interval to redraw the screen less often at higher resolutions.
    // The densityDirection is set to 'lower' when each redraw will be at a
    // lower resolution than the last image. When the resolution gets to the
    // maxiumum or minimum, the direction is reversed.
    let mills = sketch.millis();
    diffMills = mills - prevMills;
    if ((( vScale >= fastTransformThreshold ) && ( diffMills > fastTransformSpeed )) ||
      (( vScale < fastTransformThreshold ) && ( diffMills > slowTransormSpeed ))) {
      prevMills = mills;
      if (densityDirection === 'higher') {
        vScale--;
        if (vScale < 1) {
          vScale = 1;
          if (completeImageDelay) {
            completeImageDelay--;
          }
          else {
            densityDirection = 'lower';
            completeImageDelay = resetCompletImageDelay;
          }
        }
        manipulateImage( vScale, img, imageWidth, imageHeight, container );
      }
      else if (densityDirection === 'lower'){
        vScale++;
        if (vScale > vScaleMax ) {
          vScale = vScaleMax;
          densityDirection = 'higher';
        }
        manipulateImage( vScale, img, imageWidth, imageHeight, container );
      }
    }
  }

  function manipulateImage( vScale, img, imageWidth, imageHeight, container) {
    let x, y, r, b, h, index;
    let scaledX = 0.0;
    let scaledY = 0.0;
    let scaledIndex = 0;
    let scaledR = 0.0;
    let scaledB = 0.0;
    let scaledG = 0.0;
    let scaledResolution = vScale * vScale;
    let scaledHeight = Math.floor(imageHeight / vScale);
    let scaledWidth = Math.floor(imageWidth / vScale);
    let scaledImageLength = scaledHeight * scaledWidth * 4;
    let scaledPixels = new Array(scaledImageLength).fill(0);
    let manipulatedImage = sketch.createCanvas(imageWidth, imageHeight);

    //manipulatedImage.parent(container);
    sketch.image(img, 0, 0, imageWidth, imageHeight);
    sketch.loadPixels();

    // For each pixel in the image, read the associated values from the
    // pixels[] array. Store that information into the scaledPixels array.

    for (y = 0; y < imageHeight; y++ ) {
      for (x = 0; x < imageWidth; x++ ) {
        index = ( x + y * imageWidth ) * 4;
        scaledX = Math.floor(x / vScale);
        scaledY = Math.floor(y / vScale);
        if (scaledX < scaledWidth && scaledY < scaledHeight) {
          scaledIndex = ( scaledX + scaledY * scaledWidth ) * 4;
          r = sketch.pixels[index + 0];
          b = sketch.pixels[index + 1];
          g = sketch.pixels[index + 2];
          scaledR = scaledPixels[scaledIndex + 0];
          scaledB = scaledPixels[scaledIndex + 1];
          scaledG = scaledPixels[scaledIndex + 2];
          scaledR += r;
          scaledB += b;
          scaledG += g;
          scaledPixels[scaledIndex + 0] = scaledR;
          scaledPixels[scaledIndex + 1] = scaledB;
          scaledPixels[scaledIndex + 2] = scaledG;
        }
      }
    }

    // For each pixel in the image, read the scaled data in the scaledPixels
    // array and update the pixels array.

    for (y = 0; y < imageHeight; y++ ) {
      for (x = 0; x < imageWidth; x++ ) {
        index = ( x + y * imageWidth ) * 4;
        blocksPerRow = Math.floor(imageWidth / vScale);
        blocksPerColumn = Math.floor(imageHeight / vScale);
        adjustedXScale = imageWidth / blocksPerRow;
        adjustedYScale = imageHeight / blocksPerColumn;
        scaledX = Math.floor(x / adjustedXScale);
        scaledY = Math.floor(y / adjustedYScale);
        scaledIndex = ( scaledX + scaledY * scaledWidth ) * 4;
        scaledR = scaledPixels[scaledIndex + 0] / scaledResolution;
        scaledB = scaledPixels[scaledIndex + 1] / scaledResolution;
        scaledG = scaledPixels[scaledIndex + 2] / scaledResolution;
        sketch.pixels[index + 0] = Math.floor(scaledR);
        sketch.pixels[index + 1] = Math.floor(scaledB);
        sketch.pixels[index + 2] = Math.floor(scaledG);
      }
    }

    sketch.updatePixels();
  }

};

var myp5 = new p5(t, 'p5-image-resolution');
