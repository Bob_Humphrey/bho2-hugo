var
  // modules
  gulp = require('gulp'),
  del = require('del'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify-es').default,
  cleanCSS = require('gulp-clean-css'),
  imagemin = require('gulp-imagemin'),
  imageminJpegRecompress = require('imagemin-jpeg-recompress');

// JavaScript processing

gulp.task("js", function () {
    gulp.src("themes/bho2/static/js/p5-image-resolution.js")
        .pipe(uglify())
        .pipe(gulp.dest("themes/bho2/static/gulp-js/"));

    gulp.src("themes/bho2/static/js/p5-threshold.js")
        .pipe(uglify())
        .pipe(gulp.dest("themes/bho2/static/gulp-js/"));

    gulp.src("themes/bho2/static/js/p5-progressive-color.js")
        .pipe(uglify())
        .pipe(gulp.dest("themes/bho2/static/gulp-js/"));

    gulp.src("themes/bho2/static/js/p5-line-by-line.js")
        .pipe(uglify())
        .pipe(gulp.dest("themes/bho2/static/gulp-js/"));

  //The files must be in the order specified below.
  gulp.src([
    'themes/bho2/static/js/p5.min.js',
    'themes/bho2/static/js/addons/p5.dom.min.js',
    'themes/bho2/static/gulp-js/p5-image-resolution.js',
    'themes/bho2/static/gulp-js/p5-threshold.js',
    'themes/bho2/static/gulp-js/p5-progressive-color.js',
    'themes/bho2/static/gulp-js/p5-line-by-line.js'
  ])
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest('themes/bho2/static/js/'));

});

// CSS processing

gulp.task('css', function () {

    // Copy the fontawesome file to the work directory. It is already minified.
    gulp.src('themes/bho2/static/css/fontawesome-all.min.css')
        .pipe(gulp.dest('themes/bho2/static/gulp-css/'));

    // Minify the tailwinds.css file.
    gulp.src('themes/bho2/static/css/tw.css')
        .pipe(cleanCSS())
        .pipe(gulp.dest('themes/bho2/static/gulp-css/'));

    // Concatenate the files.
    gulp.src('themes/bho2/static/gulp-css/*')
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('themes/bho2/static/css/'));

});

// image processing

  //del(['themes/bho2/static/gulp-img/photography-resize/*']);


gulp.task('images1a', function () {
  gulp.src('themes/bho2/static/img-unoptimized/photography/**/*')
      .pipe(imagemin([
        imageminJpegRecompress({
          min: 50,
          max: 95,
          quality:'high'
        })
      ]))
      .pipe(gulp.dest('themes/bho2/static/photography/'))
});

// png -- took 6 minutes to minimize 48 files
gulp.task('images2', function () {
  gulp.src('C:/bho2/themes/bho2/static/img-unoptimized/projects-nonsquare/**/*')
      .pipe(imagemin([
          imagemin.optipng({optimizationLevel: 5})
        ]))
      .pipe(gulp.dest('C:/bho2/themes/bho2/static/img/projects-nonsquare/'))
});

// jpg
gulp.task('images3', function () {
  gulp.src('C:/bho2/themes/bho2/static/img-unoptimized/randall/**/*')
      .pipe(imagemin())
      .pipe(gulp.dest('C:/bho2/themes/bho2/static/img/randall/'))
});

// jpg
gulp.task('images4', function () {
  gulp.src('C:/bho2/themes/bho2/static/img-unoptimized/rex/**/*')
      .pipe(imagemin())
      .pipe(gulp.dest('C:/bho2/themes/bho2/static/img/rex/'))
});

// jpg
gulp.task('images5', function () {
  gulp.src('C:/bho2/themes/bho2/static/img-unoptimized/art/**/*')
      .pipe(imagemin())
      .pipe(gulp.dest('C:/bho2/themes/bho2/static/art/'))
});

// jpg
gulp.task('images6', function () {
  gulp.src('C:/bho2/themes/bho2/static/img-unoptimized/computer-art/**/*')
      .pipe(imagemin())
      .pipe(gulp.dest('C:/bho2/themes/bho2/static/computer-art/'))
});
